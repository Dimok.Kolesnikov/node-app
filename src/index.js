const Db = require('./dbConnect');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();

(async function general() {
    const db = new Db();

    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());

    db.connection()
        .then(() => {
            return db.synchronizing();
        })
        .then(() => {
            const {Product} = db;
            app.listen(3000, () => {
                console.log('server started at port 3000');
            })
            app.post('/addProduct', async (req, res) => {
                const {name, price} = req.body;
                await Product.create({name, price});
                res.send('success');
            })
            app.get('/getAll', async (req, res) => {
                const products = await Product.findAll()
                res.send(products);
            })
            app.get('/getCertain', async (req, res) => {
                const {name, id} = req.query;

                async function get(searchBy, value) {
                    return await Product.findAll({
                        where: {
                            [searchBy]: value
                        }
                    });
                }

                let products;
                if (id) {
                    products = await get('id', id);
                } else if (name) {
                    products = await get('name', name);
                }
                res.send(products);
            })
            app.post('/edit', async (req, res) => {
                const {id, fields} = req.body;
                await Product.update(fields, {where: {id}});
                res.send('success');
            })
            app.delete('/delete', async (req, res) => {
                const {id} = req.body;
                await Product.destroy({
                    where: {id},
                });
                res.send('success');
            })
        })

})();