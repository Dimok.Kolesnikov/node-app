const { Sequelize } = require('sequelize');
const createProduct = require('../models/product');

class Db {
    constructor() {
        this.sequelize = new Sequelize({
            "username": "Dmitry",
            "password": "12345",
            "database": "postgres",
            "host": "127.0.0.1",
            "dialect": "postgres",
            "port": "5000"
        });
    }

    async connection () {
        try {
            await this.sequelize.authenticate();
            console.log('Connectuion to database is success');
            this.Product = createProduct(this.sequelize);
            return true;
        }catch (e) {
            console.log('Connection to database is failed', e);
            return false;
        }
    }

    async synchronizing () {
        try {
            await this.Product.sync();
            console.log('Product table successfully connected');
            return true;
        } catch (e) {
            console.log('Product table did not connected', e);
            return false;
        }
    }


}

module.exports = Db;
