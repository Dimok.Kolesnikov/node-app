const { DataTypes } = require('sequelize');

function createProduct(sequelize) {
  return sequelize.define('Product', {
    // Model attributes are defined here
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    price: {
      type: DataTypes.INTEGER,
    }
  })
}

module.exports = createProduct;
